# DigitalClock
## Python digital clock gui application

**Hardware:**
- Linux PC or Raspberry Pi

**Software:**
- Linux OS or Raspberry Pi OS
- Python3
- PyGObject


**Installing system dependencies:**
- apt install libgirepository1.0-dev

**Installing modules with pip3:**
- pip3 install -r requirements.txt


**Using application:**

F11 - exit or enter full screen mode
